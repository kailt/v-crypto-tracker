import Vue from "vue";
import './plugins/axios'
import App from "./App.vue";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";

Vue.config.productionTip = false;

export const bus = new Vue();

new Vue({
  render: h => h(App)
}).$mount("#app");
